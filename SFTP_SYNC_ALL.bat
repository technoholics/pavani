@echo off
echo.
echo. 
echo ~~~~~~~~~~~~~~~~~~~~~~~~~~~ Script executed on %date% at %time% ~~~~~~~~~~~~~~~~~~~~~~~~~~~
echo.
echo ----- Moving files to backup which are older than 120days -----
echo.
forfiles /p C:\Optum\OCA\KB_Extracts\ /m *.* /s /d -365 /c "cmd /c move @file C:\Software\KB\"

TIMEOUT /T 5

echo.
echo ----- Deleting files from backupfolder which are older than 121days -----
echo.
forfiles /p "C:\Software\KB" /m * /c "cmd /c del /q @path" /d -400

TIMEOUT /T 5

option batch abort
option confirm off
echo.
echo ----- Sycnhronizing files from SFTP to the local -----
echo.
winscp.com /command "open sftp://riverside_test:Atl@nt1c0c3an@216.75.195.135:2223 -hostkey=""ssh-dss 1024 42:45:df:5c:fd:f3:6c:ce:a5:18:a0:a7:49:e9:86:63""" "synchronize local C:\Optum\OCA\KB_Extracts /CES_KB_Files" "exit"
echo.
echo.
echo ~~~~~ Files Copied Successfully ~~~~~

